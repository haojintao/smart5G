/*
 Navicat Premium Data Transfer

 Source Server         : mysql_localhost
 Source Server Type    : MySQL
 Source Server Version : 50717
 Source Host           : 127.0.0.1:3306
 Source Schema         : wisdom

 Target Server Type    : MySQL
 Target Server Version : 50717
 File Encoding         : 65001

 Date: 01/09/2019 16:03:38
*/


create database if not exists wisdom CHARACTER SET utf8 COLLATE utf8_general_ci;

use wisdom;

-- ----------------------------
-- Table structure for device
-- ----------------------------
DROP TABLE IF EXISTS `maintain`;
CREATE TABLE `maintain`  (
  `maintain_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '维修记录id',
  `service_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '营业厅id',
  `service_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '营业厅名称',
  `longitude` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '经度(和营业厅相关)',
  `latitude` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '纬度(和营业厅相关)',
  `province` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '省份信息(营业厅)',
  `city` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '市信息(营业厅)',
  `device_brand` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '设备品牌',
  `production_date` datetime(0) DEFAULT NULL COMMENT '生产日期',
  `maintain_date` datetime(0) DEFAULT NULL COMMENT '维修日期',
  `maintain_type` int(1) DEFAULT NULL COMMENT '维修类别(未定义)',
  PRIMARY KEY (`maintain_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for network
-- ----------------------------
DROP TABLE IF EXISTS `network`;
CREATE TABLE `network`  (
  `network_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '网速id',
  `device_type` int(1) DEFAULT NULL COMMENT '记录类型 设备:0  号卡:1  宽带:2',
  `day_time` datetime(0) DEFAULT NULL COMMENT '时间',
  `real_province` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '实时省',
  `real_city` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '实时市',
  `real_longitude` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '实时经度',
  `real_latitude` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '实时纬度',
  `network_speed` double(20, 0) DEFAULT NULL COMMENT '平均网速',
  `device_brand` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '设备品牌名称(针对设备)',
  PRIMARY KEY (`network_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;


DROP TRIGGER IF EXISTS `wisdom`.`network_AFTER_INSERT`;

DELIMITER $$

DROP TRIGGER IF EXISTS wisdom.network_AFTER_INSERT$$
USE `wisdom`$$
CREATE DEFINER=`root`@`localhost` TRIGGER `wisdom`.`network_AFTER_INSERT` AFTER INSERT ON `network` FOR EACH ROW
BEGIN
	insert into network_info values(concat(new.network_id, '7'), new.real_province, new.real_city, new.device_type, 7,
    (select avg(network_speed) from network
    where new.device_type = device_type and day_time between DATE_ADD(new.day_time ,INTERVAL -7 DAY) and new.day_time), new.day_time);

    insert into network_info values(concat(new.network_id, '30'), new.real_province, new.real_city, new.device_type, 30,
    (select avg(network_speed) from network
    where new.device_type = device_type and day_time between DATE_ADD(new.day_time ,INTERVAL -30 DAY) and new.day_time), new.day_time);
END$$
DELIMITER ;
-- ----------------------------
-- Table structure for network_info
-- ----------------------------
DROP TABLE IF EXISTS `network_info`;
CREATE TABLE `network_info`  (
  `info_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '网速统计信息id',
  `province` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '省',
  `city` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '市',
  `type` int(1) DEFAULT NULL COMMENT '类型 号卡:0  宽带:1',
  `info_type` int(1) DEFAULT NULL COMMENT '聚合类型(暂不定义)',
  `info_network` double(10, 0) DEFAULT NULL COMMENT '聚合网速',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`  (
  `order_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '订单id',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `age` int(3) DEFAULT NULL COMMENT '年龄',
  `sex` int(1) DEFAULT NULL COMMENT '性别:0:男 1:女',
  `price` decimal(10, 2) DEFAULT NULL COMMENT '订单价格',
  `device_brand` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '设备品牌名称(针对设备)',
  `device_type` int(1) DEFAULT NULL COMMENT '0:设备  1:号卡   2:宽带',
  `service_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '营业厅id',
  `service_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '营业厅名称',
  `longitude` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '经度(和营业厅相关)',
  `latitude` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '纬度(和营业厅相关)',
  `province` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '省份信息(营业厅)',
  `city` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '市信息(营业厅)',
  PRIMARY KEY (`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for service
-- ----------------------------
DROP TABLE IF EXISTS `service`;
CREATE TABLE `service`  (
  `service_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '营业厅id',
  `service_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '营业厅名称',
  `longitude` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '经度(和营业厅相关)',
  `latitude` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '纬度(和营业厅相关)',
  `province` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '省份信息(营业厅)',
  `city` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '市信息(营业厅)',
  PRIMARY KEY (`service_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;


DROP TABLE IF EXISTS `province_info`;
CREATE TABLE `province_info`  (
  `province_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `province_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `latitude` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `longitude` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `count1` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `count2` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`province_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of province_info
-- ----------------------------
INSERT INTO `province_info` VALUES ('110000', '北京市', '39.93170166', '116.3690033', '3166157', NULL);
INSERT INTO `province_info` VALUES ('120000', '天津市', '39.1371994', '117.1880035', '3458862', NULL);
INSERT INTO `province_info` VALUES ('130000', '河北省', '38.61384', '115.661434', '2876734', NULL);
INSERT INTO `province_info` VALUES ('140000', '山西省', '37.866566', '112.515496', '4555116', NULL);
INSERT INTO `province_info` VALUES ('150000', '内蒙古自治区', '43.468238', '114.415868', '7235027', NULL);
INSERT INTO `province_info` VALUES ('210000', '辽宁省', '41.6216', '122.753592', '5194888', NULL);
INSERT INTO `province_info` VALUES ('220000', '吉林省', '43.678846', '126.262876', '1004450', NULL);
INSERT INTO `province_info` VALUES ('230000', '黑龙江省', '47.356592', '128.047414', '8650234', NULL);
INSERT INTO `province_info` VALUES ('310000', '上海市', '31.23019981', '121.4599991', '5451064', NULL);
INSERT INTO `province_info` VALUES ('320000', '江苏省', '33.013797', '119.368489', '8860328', NULL);
INSERT INTO `province_info` VALUES ('330000', '浙江省', '29.159494', '119.957202', '6236317', NULL);
INSERT INTO `province_info` VALUES ('340000', '安徽省', '31.859252', '117.216005', '9429787', NULL);
INSERT INTO `province_info` VALUES ('350000', '福建省', '26.050118', '117.984943', '5792558', NULL);
INSERT INTO `province_info` VALUES ('360000', '江西省', '27.757258', '115.676082', '4346857', NULL);
INSERT INTO `province_info` VALUES ('370000', '山东省', '36.09929', '118.527663', '4555116', NULL);
INSERT INTO `province_info` VALUES ('410000', '河南省', '34.157184', '113.486804', '3704520', NULL);
INSERT INTO `province_info` VALUES ('420000', '湖北省', '31.209316', '112.410562', '7191884', NULL);
INSERT INTO `province_info` VALUES ('430000', '湖南省', '27.695864', '111.720664', '7377385', NULL);
INSERT INTO `province_info` VALUES ('440000', '广东省', '23.408004', '113.394818', '8068525', NULL);
INSERT INTO `province_info` VALUES ('450000', '广西壮族自治区', '23.552255', '108.924274', '3892122', NULL);
INSERT INTO `province_info` VALUES ('460000', '海南省', '19.180501', '109.733755', '4123554', NULL);
INSERT INTO `province_info` VALUES ('500000', '重庆市', '29.59049988', '106.4449997', '3556434', NULL);
INSERT INTO `province_info` VALUES ('510000', '四川省', '30.65049934', '104.0780029', '2820716', NULL);
INSERT INTO `province_info` VALUES ('520000', '贵州省', '26.902826', '106.734996', '8100530', NULL);
INSERT INTO `province_info` VALUES ('530000', '云南省', '24.864213', '101.592952', '2275982', NULL);
INSERT INTO `province_info` VALUES ('540000', '西藏自治区', '29.65239906', '91.13430023', '4940117', NULL);
INSERT INTO `province_info` VALUES ('610000', '陕西省', '35.860026', '109.503789', '6040241', NULL);
INSERT INTO `province_info` VALUES ('620000', '甘肃省', '38.103267', '102.457625', '5629465', NULL);
INSERT INTO `province_info` VALUES ('630000', '青海省', '36.60910034', '101.7839966', '6837847', NULL);
INSERT INTO `province_info` VALUES ('640000', '宁夏回族自治区', '37.321323', '106.155481', '1710139', NULL);
INSERT INTO `province_info` VALUES ('650000', '新疆维吾尔自治区', '43.79119873', '87.60379791', '9060840', NULL);
INSERT INTO `province_info` VALUES ('710000', '台湾省', '23.941928', '120.997639', '8740653', NULL);
INSERT INTO `province_info` VALUES ('810000', '香港特别行政区', '22.293586', '114.186124', '5561961', NULL);
INSERT INTO `province_info` VALUES ('820000', '澳门特别行政区', '22.204118', '113.557519', '9473122', NULL);


SET FOREIGN_KEY_CHECKS = 1;
