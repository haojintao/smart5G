package com.smart5G.service;

import com.smart5G.model.NetworkInfo;

import java.util.List;
import java.util.Map;

public interface NetworkService {

    public Map<String, List> queryNetworkInfoByCity(String province, String city, String dateType) throws Exception;

    public Map<String,List> queryNetworkInfoByProvince(String province) throws Exception;

    public Map<String, List> queryNetwAndBrandByProvince(String province) throws Exception;

    public Map<String, List> getBrandNum(String province) throws Exception;
}