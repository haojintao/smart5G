package com.smart5G.service.Impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.smart5G.service.serviceGenerator;
import com.smart5G.utils.DataSimulator;
import lombok.Data;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class serviceGeneratorImpl implements serviceGenerator {
    @Data
    public static class res implements Serializable {
        public int id;
        public String name;
        public double locationx;
        public double locationy;

        public res(int id, String name, double locationx, double locationy) {
            this.id = id;
            this.name = name;
            this.locationx = locationx;
            this.locationy = locationy;
        }
    }
    @Override
    public Object getService() {
        HashMap<String, DataSimulator.Area> hashMap = DataSimulator.readExcel();

        List<res> list = new LinkedList<>();
        List<DataSimulator.Area> areas = hashMap.values().stream().collect(Collectors.toList());
        for (DataSimulator.Area area :
                areas) {
            list.add(new res(area.code, area.name, area.longitude+(Math.random()*2-1)/4*(DataSimulator.right-DataSimulator.left)/DataSimulator.size,
                    area.latitude+(Math.random()*2-1)/4*(DataSimulator.up-DataSimulator.down)/DataSimulator.size));
        }
        return JSON.toJSON(list);
    }
}
