package com.smart5G.service.Impl;

import com.smart5G.dao.NetworkInfoMapper;
import com.smart5G.dao.NetworkMapper;
import com.smart5G.model.Network;
import com.smart5G.model.NetworkExample;
import com.smart5G.model.NetworkInfo;
import com.smart5G.model.NetworkInfoExample;
import com.smart5G.service.NetworkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class NetworkServiceImpl implements NetworkService {

    @Autowired
    private NetworkInfoMapper networkInfoMapper;

    @Autowired
    private NetworkMapper networkMapper;

    @Override
    public Map<String, List> queryNetworkInfoByCity(String province, String city, String dateType) throws Exception{
        if("".equals(province) || "".equals(city) || "".equals(dateType)){
            return null;
        }
        int infoType = 1;
        switch (dateType){
            case "d": infoType = 1;break;
            case "w": infoType = 7;break;
            case "m": infoType = 30;break;
            default: infoType = 1;
        }
        NetworkInfoExample networkInfoExample = new NetworkInfoExample();
        networkInfoExample.createCriteria().andProvinceEqualTo(province).andCityEqualTo(city)
                .andInfoTypeEqualTo(infoType).andTypeEqualTo(1);
        List<NetworkInfo> resultType1 = networkInfoMapper.selectByExample(networkInfoExample).subList(0,7);
        List<Double> mobile = new ArrayList<>();
        for(NetworkInfo networkInfo : resultType1){
            double random = Math.random();
            if(random < 0.3){
                mobile.add(networkInfo.getInfoNetwork() - 100*random);
            }else if(random < 0.6){
                mobile.add(networkInfo.getInfoNetwork() + 200*random);
            }else {
                mobile.add(networkInfo.getInfoNetwork() + 200*random);
            }
        }

        networkInfoExample = new NetworkInfoExample();
        networkInfoExample.createCriteria().andProvinceEqualTo(province).andCityEqualTo(city)
                .andInfoTypeEqualTo(infoType).andTypeEqualTo(2);
        List<NetworkInfo> resultType2 = networkInfoMapper.selectByExample(networkInfoExample).subList(0,7);
        List<Double> wideband = new ArrayList<>();
        for(NetworkInfo networkInfo : resultType2){
            double random = Math.random();
            if(random < 0.3){
                wideband.add(networkInfo.getInfoNetwork() - 30*random);
            }else if(random < 0.6){
                wideband.add(networkInfo.getInfoNetwork() + 60*random);
            }else {
                wideband.add(networkInfo.getInfoNetwork() + 60*random);
            }
        }

        Map<String, List> result = new HashMap<>();
        result.put("mobile", mobile);
        result.put("wideband", wideband);
        return result;
    }

    @Override
    public Map<String,List> queryNetworkInfoByProvince(String province) throws Exception{
        NetworkInfoExample networkInfoExample = new NetworkInfoExample();
        networkInfoExample.createCriteria().andProvinceEqualTo(province)
                .andInfoTypeEqualTo(7).andTypeEqualTo(1);
        List<NetworkInfo> resultType1 = networkInfoMapper.selectByExample(networkInfoExample).subList(0,6);
        List<Double> mobile = new ArrayList<>();
        for(NetworkInfo networkInfo : resultType1){
            double random = Math.random();
            if(random < 0.3){
                mobile.add(networkInfo.getInfoNetwork() - 100*random);
            }else if(random < 0.6){
                mobile.add(networkInfo.getInfoNetwork() + 200*random);
            }else {
                mobile.add(networkInfo.getInfoNetwork() + 200*random);
            }
        }

        networkInfoExample = new NetworkInfoExample();
        networkInfoExample.createCriteria().andProvinceEqualTo(province)
                .andInfoTypeEqualTo(30).andTypeEqualTo(2);
        List<NetworkInfo> resultType2 = networkInfoMapper.selectByExample(networkInfoExample).subList(0,6);
        List<Double> wideband = new ArrayList<>();
        for(NetworkInfo networkInfo : resultType2){
            double random = Math.random();
            if(random < 0.3){
                wideband.add(networkInfo.getInfoNetwork() - 30*random);
            }else if(random < 0.6){
                wideband.add(networkInfo.getInfoNetwork() + 60*random);
            }else {
                wideband.add(networkInfo.getInfoNetwork() + 60*random);
            }
        }

        Map<String, List> result = new HashMap<>();
        result.put("mobile", mobile);
        result.put("wideband", wideband);
        return result;
    }

    @Override
    public Map<String, List> queryNetwAndBrandByProvince(String province) throws Exception{
        List<Network> list = networkMapper.queryNetwAndBrandByProvince(province);
        List<String> brand = new ArrayList<>();
        List<Double> networkSpeed = new ArrayList<>();

        Map<String, List> result = new HashMap<>();
        for(Network networkItem : list){
            brand.add(networkItem.getDeviceBrand());
            networkSpeed.add(networkItem.getNetworkSpeed());
        }
        result.put("bread", brand);
        result.put("networkSpeed", networkSpeed);
        return result;
    }

    static Map<String, Map> brandNumResultMap = new HashMap<>();
    static {
        class Brand{
            String name;
            double rank;//品牌占比
            double random;

            public Brand(String name, double rank) {
                this.name = name;
                this.rank = rank;
            }
        }
        List<Brand> list = new LinkedList<>();
        list.add(new Brand("华为", 0.3));
        list.add(new Brand("OPPO", 0.11));
        list.add(new Brand("VIVO", 0.11));
        list.add(new Brand("小米", 0.13));
        list.add(new Brand("苹果", 0.26));
        list.add(new Brand("三星", 0.03));
        list.add(new Brand("思科", 0.03));
        list.add(new Brand("其他", 0.03));

        List<String> provinces = new LinkedList<>();
        provinces.add("北京市");
        provinces.add("山东省");
        provinces.add("江苏省");

        for (String province: provinces) {
            List<String> brand = new ArrayList<>();
            List<Double> num = new ArrayList<>();

            double sum = 0;
            for (Brand brands : list) {
                brands.random = brands.rank*(1+Math.random()*0.6-0.3);
                sum += brands.random;
            }
            for (Brand brands : list) {
                brands.random /= sum;
                brand.add(brands.name);
                num.add(brands.random);
            }
            Map<String, List> provinceMap = new HashMap<>();
            provinceMap.put("brand", brand);
            provinceMap.put("num", num);
            brandNumResultMap.put(province, provinceMap) ;
        }

    }

    @Override
    public Map<String, List> getBrandNum(String province) throws Exception{
        Map<String, List> resultMap = brandNumResultMap.get(province);

        return resultMap;
    }

}
