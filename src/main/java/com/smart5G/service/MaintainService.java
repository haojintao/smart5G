package com.smart5G.service;


import com.smart5G.dao.MaintainMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

@Service
public class MaintainService {

    @Autowired
    MaintainMapper maintainMapper;

    public BigDecimal getAllMaintenanceAmount(String province){
        BigDecimal bigDecimal = maintainMapper.getAllMaintenanceAmount(province);
        return bigDecimal;
    }
    public ArrayList<Map<String,Integer>> getBrandMaintenanceAmount(String province) {
        ArrayList<Map<String,Integer>> list=null;
        list=maintainMapper.getBrandMaintenanceAmount(province);
       // System.out.println(list);
        return list;
    }

    public Map<String, List> getMaintenanceAmountByDate(String serviceId, String dateType) {
        Map<String,List> data=new HashMap<>(2);
        switch (dateType){
            case ("d"):
                ArrayList<Map<String,Object>> list1=maintainMapper.getMaintenanceAmountByDay(serviceId);
                data=traverseMap(list1,"days");
                break;
            case ("w"):
                ArrayList<Map<String,Object>> list2=maintainMapper.getMaintenanceAmountByWeek(serviceId);
                data=traverseMap(list2,"days");
                break;
            case ("m"):
                ArrayList<Map<String,Object>> list3=maintainMapper.getMaintenanceAmountByMonth(serviceId);
                data=traverseMap(list3,"days");
                break;
        }
        return data;
    }

    public Map<String,List> traverseMap(ArrayList<Map<String,Object>> list,String dateType){
        Map<String,List> data=new HashMap<>(2);
        List<String> dateList=new ArrayList<>(7);
        List<Long> MaintainCount=new ArrayList<>(7);
        for(int i=0;i<7;i++){
            if(Math.random() < 0.3){
                MaintainCount.add(1L);
            }else if(Math.random() < 0.6){
                MaintainCount.add(2L);
            }else {
                MaintainCount.add(3L);
            }
        }
        for(Map<String,Object> map: list){
            System.out.println(map+"   "+map.size());
            Iterator<Map.Entry<String, Object>> iterator = map.entrySet().iterator();
            while (iterator.hasNext()){
                Map.Entry<String, Object> next = iterator.next();
                String key = next.getKey();
                if("count".equals(key)){
                    MaintainCount.add((Long) next.getValue());
                }else{
                    dateList.add((String) next.getValue());
                }
            }
        }
        data.put("dateList",dateList);
        data.put("MaintainCount",MaintainCount);
        return data;
    }

}
