package com.smart5G.service;

import com.smart5G.dao.OrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

@Service
public class OrderService {

    @Autowired
    OrderMapper orderMapper;

    public BigDecimal getAllSaleroom(String province) {
        BigDecimal bigDecimal=orderMapper.getAllSaleroom(province);
        return bigDecimal;
    }

    public ArrayList<Map<String,Integer>> getBrandQuantity(String province) {
        ArrayList<Map<String,Integer>> list=null;
        list=orderMapper.getBrandQuantity(province);
        System.out.println(list);
        return list;
    }

    public Map<String,List> getOrderMashup(String serviceId, String dateType) {
        Map<String,List> data=new HashMap<>(2);
        switch (dateType){
            case ("d"):
                ArrayList<Map<String,Object>> list1=orderMapper.getOrderMashupByDay(serviceId);
                data=traverseMap(list1,"days");
                break;
            case ("w"):
                ArrayList<Map<String,Object>> list2=orderMapper.getOrderMashupByWeek(serviceId);
                data=traverseMap(list2,"days");
                break;
            case ("m"):
                ArrayList<Map<String,Object>> list3=orderMapper.getOrderMashupByMonth(serviceId);
                data=traverseMap(list3,"days");
                break;
        }
        return data;
    }

    /**
     * 对map数据进行封装方法
     * @param list
     * @return
     */
    public Map<String,List> traverseMap(ArrayList<Map<String,Object>> list,String dateType){
        Map<String,List> data=new HashMap<>(2);
        List<String> dateList=new ArrayList<>(7);
        List<Object> orderCount=new ArrayList<>(7);
        for(Map<String,Object> map: list){
            System.out.println(map+"   "+map.size());
            Iterator<Map.Entry<String, Object>> iterator = map.entrySet().iterator();
            while (iterator.hasNext()){
                Map.Entry<String, Object> next = iterator.next();
                String key = next.getKey();
                if("count".equals(key)){
                    orderCount.add(next.getValue());
                }else{
                    dateList.add((String) next.getValue());
                }
            }
        }
        data.put("dateList",dateList);
        data.put("orderCount",orderCount);
        return data;
    }

    public Map<String,Object> getGenderData(String province) {
        Map<String,Object> map=new HashMap<>();
        List<Integer> manList=new ArrayList<>();
        List<Integer> womanList=new ArrayList<>();
//        1.获取该省的男性用户
        int manCount=orderMapper.getManCount(province);
//        2.获取该省的女性用户
        int womanCount=orderMapper.getWomanCount(province);
        int sumCount=manCount+womanCount;
        String manAccuracy = accuracy(manCount, sumCount, 2);
        String womanAccuracy = accuracy(womanCount, sumCount, 2);
        map.put("manCount",manCount);
        map.put("womanCount",womanCount);
        map.put("manAccuracy",manAccuracy);
        map.put("womanAccuracy",womanAccuracy);
//        3.获取<=20的男
        int man1=orderMapper.getMan1(province);
        manList.add(man1);
//        4.获取<=20的女
        int woman1=orderMapper.getWoman1(province);
        womanList.add(woman1);
//        5.获取20<age<=30的男
        int man2=orderMapper.getMan2(province);
        manList.add(man2);
//        6.获取20<age<=30的女
        int woman2=orderMapper.getWoman2(province);
        womanList.add(woman2);
//        7.获取30<age<=40的男
        int man3=orderMapper.getMan3(province);
        manList.add(man3);
//        8.获取30<age<=40的女
        int woman3=orderMapper.getWoman3(province);
        womanList.add(woman3);
//        9.获取40<age<=50的男
        int man4=orderMapper.getMan4(province);
        manList.add(man4);
//        10.获取40<age<=50的女
        int woman4=orderMapper.getWoman4(province);
        womanList.add(woman4);
//        11.获取>50的男
        int man5=orderMapper.getMan5(province);
        manList.add(man5);
//        12.获取>50的女
        int woman5=orderMapper.getWoman5(province);
        womanList.add(woman5);
        map.put("manList",manList);
        map.put("womanList",womanList);
        return map;
    }

    /**
     * 计算百分比
     * @param num
     * @param total
     * @param scale
     * @return
     */
    public static String accuracy(double num, double total, int scale){
        DecimalFormat df = (DecimalFormat)NumberFormat.getInstance();
        //可以设置精确几位小数
        df.setMaximumFractionDigits(scale);
        //模式 例如四舍五入
        df.setRoundingMode(RoundingMode.HALF_UP);
        double accuracy_num = num / total * 100;
        return df.format(accuracy_num)+"%";
    }

    public List<Map<String,String>> getAllProvinceData() {
        List<Map<String,String>> list=new ArrayList<>();
        list=orderMapper.getAllProvinceData();
        return list;
    }
}
