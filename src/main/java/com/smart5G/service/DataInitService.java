package com.smart5G.service;


import com.alibaba.fastjson.JSONArray;
import com.smart5G.model.Service;

import java.util.List;

public interface DataInitService {
    Boolean initService();

    Boolean initOrder();

    Boolean initMaintain();

    Boolean initFlow();

    Boolean initAggregateFlow();

    List<Service> queryService();
}
