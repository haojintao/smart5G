package com.smart5G.controller;

import com.smart5G.model.NetworkInfo;
import com.smart5G.service.NetworkService;
import com.smart5G.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import sun.nio.ch.Net;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/network")
public class NetworkController {

    @Autowired
    private NetworkService networkService;

    @RequestMapping(value = "/queryNetworkInfoByCity")
    public Result queryNetworkInfoByCity(@RequestParam("province")String province,
                                         @RequestParam("city")String city,
                                         @RequestParam("dateType")String dateType){
        System.out.println("####### queryNetworkInfoByCity: " + province + "," + city + "," + dateType + " ######");
        try{
            Map<String, List> map = networkService.queryNetworkInfoByCity(province, city, dateType);
            if(null != map){
                return Result.success(map);
            }else {
                return Result.fail("9999","获取网速列表信息失败");
            }
        }catch (Exception e){
            return Result.fail("9999","获取网速列表信息失败");
        }

    }

    @RequestMapping(value = "/queryNetworkInfoByProvince")
    public Result queryNetworkInfoByProvince(@RequestParam("province")String province){
        System.out.println("####### queryNetworkInfoByProvince: " + province + " ######");
        try{
            Map<String, List> map = networkService.queryNetworkInfoByProvince(province);
            if(null != map){
                return Result.success(map);
            }else {
                return Result.fail("9999","获取网速列表信息失败");
            }
        }catch (Exception e){
            return Result.fail("9999","获取网速列表信息失败");
        }

    }

    @RequestMapping(value = "/queryNetwAndBrandByProvince")
    public Result queryNetwAndBrandByProvince(@RequestParam("province")String province){
        System.out.println("####### queryNetwAndBrandByProvince: " + province + " ######");
        try{
            Map<String, List> map = networkService.queryNetwAndBrandByProvince(province);
            if(null != map){
                return Result.success(map);
            }else {
                return Result.fail("9999","获取网速列表信息失败");
            }
        }catch (Exception e){
            return Result.fail("9999","获取网速列表信息失败");
        }
    }


}
