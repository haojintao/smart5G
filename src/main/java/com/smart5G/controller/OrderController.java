package com.smart5G.controller;

import com.smart5G.service.OrderService;
import com.smart5G.utils.Result;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    OrderService orderService;

    @RequestMapping(value="/allSaleroom",method = RequestMethod.GET)
    public Result allSaleroom(@RequestParam("location") String province){
        System.out.println("order/allSaleroom   province="+province);
        try {
            BigDecimal bigDecimal =orderService.getAllSaleroom(province);
            if(bigDecimal!=null){
                System.out.println(bigDecimal);
                return Result.success(bigDecimal);
            }else {
                return Result.fail("9001","获取总销售额发生异常");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Result.fail("9001","获取总销售额发生异常");
        }
    }
    @RequestMapping(value="/brandQuantity",method = RequestMethod.GET)
    public Result brandQuantity(@RequestParam("location") String province){
        System.out.println("order/brandQuantity   province="+province);
        try {
            ArrayList<Map<String,Integer>> list=orderService.getBrandQuantity(province);
            if(list!=null){
                return Result.success(list);
            }else{
                return Result.fail("9002","获取地区销售品牌数量发生异常");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Result.fail("9002","获取地区销售品牌数量发生异常");
        }
    }

    @RequestMapping(value="/orderMashup",method = RequestMethod.GET)
    public Result orderMashup(String serviceId,String dateType){
        System.out.println("order/orderMashup   serviceId="+serviceId+"     dateType="+dateType);
        try {
            Map<String,List> data=orderService.getOrderMashup(serviceId,dateType);
            if(data!=null){
                return Result.success(data);
            }else{
                return Result.fail("9003","获取订单信息发生异常");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Result.fail("9003","获取订单信息发生异常");
        }
    }
    @RequestMapping(value = "/genderData",method = RequestMethod.GET)
    public Result genderData(String province){
        try {
            Map<String,Object> data=orderService.getGenderData(province);
            if(data!=null){
                return Result.success(data);
            }else{
                return Result.fail("9004","获取性别信息发生异常");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Result.fail("9004","获取性别信息发生异常");
        }
    }
    @RequestMapping(value = "/allProvinceData",method = RequestMethod.GET)
    public Result allProvinceData(){
        try {
            List<Map<String,String>> data=orderService.getAllProvinceData();
            if(data!=null){
                return Result.success(data);
            }else{
                return Result.fail("9005","获取全省信息发生异常");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Result.fail("9005","获取全省信息发生异常");
        }
    }
}
