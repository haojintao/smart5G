package com.smart5G.controller;

import com.smart5G.model.Network;
import com.smart5G.service.NetworkService;
import com.smart5G.service.serviceGenerator;
import com.smart5G.service.DataInitService;
import com.smart5G.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/test")
public class Test {

    @Autowired
    DataInitService dataInitService;

    @Autowired
    NetworkService networkService;

    @RequestMapping(value = "/service", method = {RequestMethod.POST,RequestMethod.GET})
    public Result queryService(){
        return Result.success(dataInitService.queryService());
    }

    @RequestMapping(value = "/init", method = {RequestMethod.POST,RequestMethod.GET})
    public Result initData(){
        //初始化营业厅位置
       if (!dataInitService.initService()){
            return Result.fail("9999", "初始化营业厅位置失败");
        }
//        //初始化订单
//        if (!dataInitService.initOrder()){
//            return Result.fail("9999", "初始化订单位置失败");
//        }
//        //初始化维修表
//        if (!dataInitService.initMaintain()){
//            return Result.fail("9999", "初始化维修单失败");
//        }
//        //初始化流量
//        if (!dataInitService.initFlow()){
//            return Result.fail("9999", "初始化流量失败");
//        }
        return Result.success(true);
    }

    @RequestMapping(value = "/brandNum")
    public Result getBrandNum(@RequestParam("province")String province){

        System.out.println("####### getBrandNum: " + province + " ######");
        try{
            Map<String, List> map = networkService.getBrandNum(province);
            if(null != map){
                return Result.success(map);
            }else {
                return Result.fail("9999","获取品牌网速失败");
            }
        }catch (Exception e){
            return Result.fail("9999","获取品牌网速失败");
        }
    }
}
