package com.smart5G.controller;

import com.smart5G.service.MaintainService;
import com.smart5G.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/maintain")
public class MaintainController {
    @Autowired
    MaintainService maintainService;

    @RequestMapping(value="/allMaintenanceAmount",method = RequestMethod.GET)
    public Result allMaintenanceAmount(@RequestParam("location") String province){
        System.out.println("maintain/allMaintenanceAmount   province="+province);
        try {
            BigDecimal bigDecimal =maintainService.getAllMaintenanceAmount(province);
            if(bigDecimal!=null){
                System.out.println(bigDecimal);
                return Result.success(bigDecimal);
            }else {
                return Result.fail("8001","获取"+province+"总维修量发生异常");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Result.fail("8001","获取"+province+"总维修量发生异常");
        }
    }

    @RequestMapping(value="/brandMaintenanceAmount",method = RequestMethod.GET)
    public Result brandMaintenanceAmount(@RequestParam("location") String province){
        System.out.println("maintain/brandMaintenanceAmount   province="+province);
        try {
            ArrayList<Map<String,Integer>> list=maintainService.getBrandMaintenanceAmount(province);
            ArrayList<Map<String,Integer>> list1 = null;
            int sum = 0;

            if(list!=null){
                return Result.success(list);
            }else{
                return Result.fail("8002","获取"+province+"地区维修品牌数量发生异常");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Result.fail("8002","获取"+province+"地区维修品牌数量发生异常");
        }
    }

    @RequestMapping(value="/maintenanceAmountByDate",method = RequestMethod.GET)
    public Result maintenanceAmountByDate(String serviceId,String dateType){
        System.out.println("maintain/maintenanceAmountByDate   serviceId="+serviceId+" dateType="+dateType);
        try {
            Map<String, List> data=maintainService.getMaintenanceAmountByDate(serviceId,dateType);
            if(data!=null){
                return Result.success(data);
            }else{
                return Result.fail("8003","获取维修量信息发生异常");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Result.fail("8003","获取维修量信息发生异常");
        }
    }

}
