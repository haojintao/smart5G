package com.smart5G.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class NetworkExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public NetworkExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andNetworkIdIsNull() {
            addCriterion("network_id is null");
            return (Criteria) this;
        }

        public Criteria andNetworkIdIsNotNull() {
            addCriterion("network_id is not null");
            return (Criteria) this;
        }

        public Criteria andNetworkIdEqualTo(String value) {
            addCriterion("network_id =", value, "networkId");
            return (Criteria) this;
        }

        public Criteria andNetworkIdNotEqualTo(String value) {
            addCriterion("network_id <>", value, "networkId");
            return (Criteria) this;
        }

        public Criteria andNetworkIdGreaterThan(String value) {
            addCriterion("network_id >", value, "networkId");
            return (Criteria) this;
        }

        public Criteria andNetworkIdGreaterThanOrEqualTo(String value) {
            addCriterion("network_id >=", value, "networkId");
            return (Criteria) this;
        }

        public Criteria andNetworkIdLessThan(String value) {
            addCriterion("network_id <", value, "networkId");
            return (Criteria) this;
        }

        public Criteria andNetworkIdLessThanOrEqualTo(String value) {
            addCriterion("network_id <=", value, "networkId");
            return (Criteria) this;
        }

        public Criteria andNetworkIdLike(String value) {
            addCriterion("network_id like", value, "networkId");
            return (Criteria) this;
        }

        public Criteria andNetworkIdNotLike(String value) {
            addCriterion("network_id not like", value, "networkId");
            return (Criteria) this;
        }

        public Criteria andNetworkIdIn(List<String> values) {
            addCriterion("network_id in", values, "networkId");
            return (Criteria) this;
        }

        public Criteria andNetworkIdNotIn(List<String> values) {
            addCriterion("network_id not in", values, "networkId");
            return (Criteria) this;
        }

        public Criteria andNetworkIdBetween(String value1, String value2) {
            addCriterion("network_id between", value1, value2, "networkId");
            return (Criteria) this;
        }

        public Criteria andNetworkIdNotBetween(String value1, String value2) {
            addCriterion("network_id not between", value1, value2, "networkId");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeIsNull() {
            addCriterion("device_type is null");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeIsNotNull() {
            addCriterion("device_type is not null");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeEqualTo(Integer value) {
            addCriterion("device_type =", value, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeNotEqualTo(Integer value) {
            addCriterion("device_type <>", value, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeGreaterThan(Integer value) {
            addCriterion("device_type >", value, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("device_type >=", value, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeLessThan(Integer value) {
            addCriterion("device_type <", value, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeLessThanOrEqualTo(Integer value) {
            addCriterion("device_type <=", value, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeIn(List<Integer> values) {
            addCriterion("device_type in", values, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeNotIn(List<Integer> values) {
            addCriterion("device_type not in", values, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeBetween(Integer value1, Integer value2) {
            addCriterion("device_type between", value1, value2, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDeviceTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("device_type not between", value1, value2, "deviceType");
            return (Criteria) this;
        }

        public Criteria andDayTimeIsNull() {
            addCriterion("day_time is null");
            return (Criteria) this;
        }

        public Criteria andDayTimeIsNotNull() {
            addCriterion("day_time is not null");
            return (Criteria) this;
        }

        public Criteria andDayTimeEqualTo(Date value) {
            addCriterion("day_time =", value, "dayTime");
            return (Criteria) this;
        }

        public Criteria andDayTimeNotEqualTo(Date value) {
            addCriterion("day_time <>", value, "dayTime");
            return (Criteria) this;
        }

        public Criteria andDayTimeGreaterThan(Date value) {
            addCriterion("day_time >", value, "dayTime");
            return (Criteria) this;
        }

        public Criteria andDayTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("day_time >=", value, "dayTime");
            return (Criteria) this;
        }

        public Criteria andDayTimeLessThan(Date value) {
            addCriterion("day_time <", value, "dayTime");
            return (Criteria) this;
        }

        public Criteria andDayTimeLessThanOrEqualTo(Date value) {
            addCriterion("day_time <=", value, "dayTime");
            return (Criteria) this;
        }

        public Criteria andDayTimeIn(List<Date> values) {
            addCriterion("day_time in", values, "dayTime");
            return (Criteria) this;
        }

        public Criteria andDayTimeNotIn(List<Date> values) {
            addCriterion("day_time not in", values, "dayTime");
            return (Criteria) this;
        }

        public Criteria andDayTimeBetween(Date value1, Date value2) {
            addCriterion("day_time between", value1, value2, "dayTime");
            return (Criteria) this;
        }

        public Criteria andDayTimeNotBetween(Date value1, Date value2) {
            addCriterion("day_time not between", value1, value2, "dayTime");
            return (Criteria) this;
        }

        public Criteria andRealProvinceIsNull() {
            addCriterion("real_province is null");
            return (Criteria) this;
        }

        public Criteria andRealProvinceIsNotNull() {
            addCriterion("real_province is not null");
            return (Criteria) this;
        }

        public Criteria andRealProvinceEqualTo(String value) {
            addCriterion("real_province =", value, "realProvince");
            return (Criteria) this;
        }

        public Criteria andRealProvinceNotEqualTo(String value) {
            addCriterion("real_province <>", value, "realProvince");
            return (Criteria) this;
        }

        public Criteria andRealProvinceGreaterThan(String value) {
            addCriterion("real_province >", value, "realProvince");
            return (Criteria) this;
        }

        public Criteria andRealProvinceGreaterThanOrEqualTo(String value) {
            addCriterion("real_province >=", value, "realProvince");
            return (Criteria) this;
        }

        public Criteria andRealProvinceLessThan(String value) {
            addCriterion("real_province <", value, "realProvince");
            return (Criteria) this;
        }

        public Criteria andRealProvinceLessThanOrEqualTo(String value) {
            addCriterion("real_province <=", value, "realProvince");
            return (Criteria) this;
        }

        public Criteria andRealProvinceLike(String value) {
            addCriterion("real_province like", value, "realProvince");
            return (Criteria) this;
        }

        public Criteria andRealProvinceNotLike(String value) {
            addCriterion("real_province not like", value, "realProvince");
            return (Criteria) this;
        }

        public Criteria andRealProvinceIn(List<String> values) {
            addCriterion("real_province in", values, "realProvince");
            return (Criteria) this;
        }

        public Criteria andRealProvinceNotIn(List<String> values) {
            addCriterion("real_province not in", values, "realProvince");
            return (Criteria) this;
        }

        public Criteria andRealProvinceBetween(String value1, String value2) {
            addCriterion("real_province between", value1, value2, "realProvince");
            return (Criteria) this;
        }

        public Criteria andRealProvinceNotBetween(String value1, String value2) {
            addCriterion("real_province not between", value1, value2, "realProvince");
            return (Criteria) this;
        }

        public Criteria andRealCityIsNull() {
            addCriterion("real_city is null");
            return (Criteria) this;
        }

        public Criteria andRealCityIsNotNull() {
            addCriterion("real_city is not null");
            return (Criteria) this;
        }

        public Criteria andRealCityEqualTo(String value) {
            addCriterion("real_city =", value, "realCity");
            return (Criteria) this;
        }

        public Criteria andRealCityNotEqualTo(String value) {
            addCriterion("real_city <>", value, "realCity");
            return (Criteria) this;
        }

        public Criteria andRealCityGreaterThan(String value) {
            addCriterion("real_city >", value, "realCity");
            return (Criteria) this;
        }

        public Criteria andRealCityGreaterThanOrEqualTo(String value) {
            addCriterion("real_city >=", value, "realCity");
            return (Criteria) this;
        }

        public Criteria andRealCityLessThan(String value) {
            addCriterion("real_city <", value, "realCity");
            return (Criteria) this;
        }

        public Criteria andRealCityLessThanOrEqualTo(String value) {
            addCriterion("real_city <=", value, "realCity");
            return (Criteria) this;
        }

        public Criteria andRealCityLike(String value) {
            addCriterion("real_city like", value, "realCity");
            return (Criteria) this;
        }

        public Criteria andRealCityNotLike(String value) {
            addCriterion("real_city not like", value, "realCity");
            return (Criteria) this;
        }

        public Criteria andRealCityIn(List<String> values) {
            addCriterion("real_city in", values, "realCity");
            return (Criteria) this;
        }

        public Criteria andRealCityNotIn(List<String> values) {
            addCriterion("real_city not in", values, "realCity");
            return (Criteria) this;
        }

        public Criteria andRealCityBetween(String value1, String value2) {
            addCriterion("real_city between", value1, value2, "realCity");
            return (Criteria) this;
        }

        public Criteria andRealCityNotBetween(String value1, String value2) {
            addCriterion("real_city not between", value1, value2, "realCity");
            return (Criteria) this;
        }

        public Criteria andRealLongitudeIsNull() {
            addCriterion("real_longitude is null");
            return (Criteria) this;
        }

        public Criteria andRealLongitudeIsNotNull() {
            addCriterion("real_longitude is not null");
            return (Criteria) this;
        }

        public Criteria andRealLongitudeEqualTo(String value) {
            addCriterion("real_longitude =", value, "realLongitude");
            return (Criteria) this;
        }

        public Criteria andRealLongitudeNotEqualTo(String value) {
            addCriterion("real_longitude <>", value, "realLongitude");
            return (Criteria) this;
        }

        public Criteria andRealLongitudeGreaterThan(String value) {
            addCriterion("real_longitude >", value, "realLongitude");
            return (Criteria) this;
        }

        public Criteria andRealLongitudeGreaterThanOrEqualTo(String value) {
            addCriterion("real_longitude >=", value, "realLongitude");
            return (Criteria) this;
        }

        public Criteria andRealLongitudeLessThan(String value) {
            addCriterion("real_longitude <", value, "realLongitude");
            return (Criteria) this;
        }

        public Criteria andRealLongitudeLessThanOrEqualTo(String value) {
            addCriterion("real_longitude <=", value, "realLongitude");
            return (Criteria) this;
        }

        public Criteria andRealLongitudeLike(String value) {
            addCriterion("real_longitude like", value, "realLongitude");
            return (Criteria) this;
        }

        public Criteria andRealLongitudeNotLike(String value) {
            addCriterion("real_longitude not like", value, "realLongitude");
            return (Criteria) this;
        }

        public Criteria andRealLongitudeIn(List<String> values) {
            addCriterion("real_longitude in", values, "realLongitude");
            return (Criteria) this;
        }

        public Criteria andRealLongitudeNotIn(List<String> values) {
            addCriterion("real_longitude not in", values, "realLongitude");
            return (Criteria) this;
        }

        public Criteria andRealLongitudeBetween(String value1, String value2) {
            addCriterion("real_longitude between", value1, value2, "realLongitude");
            return (Criteria) this;
        }

        public Criteria andRealLongitudeNotBetween(String value1, String value2) {
            addCriterion("real_longitude not between", value1, value2, "realLongitude");
            return (Criteria) this;
        }

        public Criteria andRealLatitudeIsNull() {
            addCriterion("real_latitude is null");
            return (Criteria) this;
        }

        public Criteria andRealLatitudeIsNotNull() {
            addCriterion("real_latitude is not null");
            return (Criteria) this;
        }

        public Criteria andRealLatitudeEqualTo(String value) {
            addCriterion("real_latitude =", value, "realLatitude");
            return (Criteria) this;
        }

        public Criteria andRealLatitudeNotEqualTo(String value) {
            addCriterion("real_latitude <>", value, "realLatitude");
            return (Criteria) this;
        }

        public Criteria andRealLatitudeGreaterThan(String value) {
            addCriterion("real_latitude >", value, "realLatitude");
            return (Criteria) this;
        }

        public Criteria andRealLatitudeGreaterThanOrEqualTo(String value) {
            addCriterion("real_latitude >=", value, "realLatitude");
            return (Criteria) this;
        }

        public Criteria andRealLatitudeLessThan(String value) {
            addCriterion("real_latitude <", value, "realLatitude");
            return (Criteria) this;
        }

        public Criteria andRealLatitudeLessThanOrEqualTo(String value) {
            addCriterion("real_latitude <=", value, "realLatitude");
            return (Criteria) this;
        }

        public Criteria andRealLatitudeLike(String value) {
            addCriterion("real_latitude like", value, "realLatitude");
            return (Criteria) this;
        }

        public Criteria andRealLatitudeNotLike(String value) {
            addCriterion("real_latitude not like", value, "realLatitude");
            return (Criteria) this;
        }

        public Criteria andRealLatitudeIn(List<String> values) {
            addCriterion("real_latitude in", values, "realLatitude");
            return (Criteria) this;
        }

        public Criteria andRealLatitudeNotIn(List<String> values) {
            addCriterion("real_latitude not in", values, "realLatitude");
            return (Criteria) this;
        }

        public Criteria andRealLatitudeBetween(String value1, String value2) {
            addCriterion("real_latitude between", value1, value2, "realLatitude");
            return (Criteria) this;
        }

        public Criteria andRealLatitudeNotBetween(String value1, String value2) {
            addCriterion("real_latitude not between", value1, value2, "realLatitude");
            return (Criteria) this;
        }

        public Criteria andNetworkSpeedIsNull() {
            addCriterion("network_speed is null");
            return (Criteria) this;
        }

        public Criteria andNetworkSpeedIsNotNull() {
            addCriterion("network_speed is not null");
            return (Criteria) this;
        }

        public Criteria andNetworkSpeedEqualTo(Double value) {
            addCriterion("network_speed =", value, "networkSpeed");
            return (Criteria) this;
        }

        public Criteria andNetworkSpeedNotEqualTo(Double value) {
            addCriterion("network_speed <>", value, "networkSpeed");
            return (Criteria) this;
        }

        public Criteria andNetworkSpeedGreaterThan(Double value) {
            addCriterion("network_speed >", value, "networkSpeed");
            return (Criteria) this;
        }

        public Criteria andNetworkSpeedGreaterThanOrEqualTo(Double value) {
            addCriterion("network_speed >=", value, "networkSpeed");
            return (Criteria) this;
        }

        public Criteria andNetworkSpeedLessThan(Double value) {
            addCriterion("network_speed <", value, "networkSpeed");
            return (Criteria) this;
        }

        public Criteria andNetworkSpeedLessThanOrEqualTo(Double value) {
            addCriterion("network_speed <=", value, "networkSpeed");
            return (Criteria) this;
        }

        public Criteria andNetworkSpeedIn(List<Double> values) {
            addCriterion("network_speed in", values, "networkSpeed");
            return (Criteria) this;
        }

        public Criteria andNetworkSpeedNotIn(List<Double> values) {
            addCriterion("network_speed not in", values, "networkSpeed");
            return (Criteria) this;
        }

        public Criteria andNetworkSpeedBetween(Double value1, Double value2) {
            addCriterion("network_speed between", value1, value2, "networkSpeed");
            return (Criteria) this;
        }

        public Criteria andNetworkSpeedNotBetween(Double value1, Double value2) {
            addCriterion("network_speed not between", value1, value2, "networkSpeed");
            return (Criteria) this;
        }

        public Criteria andDeviceBrandIsNull() {
            addCriterion("device_brand is null");
            return (Criteria) this;
        }

        public Criteria andDeviceBrandIsNotNull() {
            addCriterion("device_brand is not null");
            return (Criteria) this;
        }

        public Criteria andDeviceBrandEqualTo(String value) {
            addCriterion("device_brand =", value, "deviceBrand");
            return (Criteria) this;
        }

        public Criteria andDeviceBrandNotEqualTo(String value) {
            addCriterion("device_brand <>", value, "deviceBrand");
            return (Criteria) this;
        }

        public Criteria andDeviceBrandGreaterThan(String value) {
            addCriterion("device_brand >", value, "deviceBrand");
            return (Criteria) this;
        }

        public Criteria andDeviceBrandGreaterThanOrEqualTo(String value) {
            addCriterion("device_brand >=", value, "deviceBrand");
            return (Criteria) this;
        }

        public Criteria andDeviceBrandLessThan(String value) {
            addCriterion("device_brand <", value, "deviceBrand");
            return (Criteria) this;
        }

        public Criteria andDeviceBrandLessThanOrEqualTo(String value) {
            addCriterion("device_brand <=", value, "deviceBrand");
            return (Criteria) this;
        }

        public Criteria andDeviceBrandLike(String value) {
            addCriterion("device_brand like", value, "deviceBrand");
            return (Criteria) this;
        }

        public Criteria andDeviceBrandNotLike(String value) {
            addCriterion("device_brand not like", value, "deviceBrand");
            return (Criteria) this;
        }

        public Criteria andDeviceBrandIn(List<String> values) {
            addCriterion("device_brand in", values, "deviceBrand");
            return (Criteria) this;
        }

        public Criteria andDeviceBrandNotIn(List<String> values) {
            addCriterion("device_brand not in", values, "deviceBrand");
            return (Criteria) this;
        }

        public Criteria andDeviceBrandBetween(String value1, String value2) {
            addCriterion("device_brand between", value1, value2, "deviceBrand");
            return (Criteria) this;
        }

        public Criteria andDeviceBrandNotBetween(String value1, String value2) {
            addCriterion("device_brand not between", value1, value2, "deviceBrand");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}