package com.smart5G.model;

import java.util.Date;

public class NetworkInfo {
    private String infoId;

    private String province;

    private String city;

    private Integer type;

    private Integer infoType;

    private Double infoNetwork;

    private Date createTime;

    public String getInfoId() {
        return infoId;
    }

    public void setInfoId(String infoId) {
        this.infoId = infoId == null ? null : infoId.trim();
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getInfoType() {
        return infoType;
    }

    public void setInfoType(Integer infoType) {
        this.infoType = infoType;
    }

    public Double getInfoNetwork() {
        return infoNetwork;
    }

    public void setInfoNetwork(Double infoNetwork) {
        this.infoNetwork = infoNetwork;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}