package com.smart5G.model;

import java.util.Date;


public class Network {
    private String networkId;

    private Integer deviceType;

    private Date dayTime;

    private String realProvince;

    private String realCity;

    private String realLongitude;

    private String realLatitude;

    private Double networkSpeed;

    private String deviceBrand;

    public String getNetworkId() {
        return networkId;
    }

    public void setNetworkId(String networkId) {
        this.networkId = networkId == null ? null : networkId.trim();
    }

    public Integer getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(Integer deviceType) {
        this.deviceType = deviceType;
    }

    public Date getDayTime() {
        return dayTime;
    }

    public void setDayTime(Date dayTime) {
        this.dayTime = dayTime;
    }

    public String getRealProvince() {
        return realProvince;
    }

    public void setRealProvince(String realProvince) {
        this.realProvince = realProvince == null ? null : realProvince.trim();
    }

    public String getRealCity() {
        return realCity;
    }

    public void setRealCity(String realCity) {
        this.realCity = realCity == null ? null : realCity.trim();
    }

    public String getRealLongitude() {
        return realLongitude;
    }

    public void setRealLongitude(String realLongitude) {
        this.realLongitude = realLongitude == null ? null : realLongitude.trim();
    }

    public String getRealLatitude() {
        return realLatitude;
    }

    public void setRealLatitude(String realLatitude) {
        this.realLatitude = realLatitude == null ? null : realLatitude.trim();
    }

    public Double getNetworkSpeed() {
        return networkSpeed;
    }

    public void setNetworkSpeed(Double networkSpeed) {
        this.networkSpeed = networkSpeed;
    }

    public String getDeviceBrand() {
        return deviceBrand;
    }

    public void setDeviceBrand(String deviceBrand) {
        this.deviceBrand = deviceBrand == null ? null : deviceBrand.trim();
    }
}