package com.smart5G.utils;

public class PageUtil {
    private static int pageNum = 0;

    private static int pageSize = 0;

    private static int totalRecord;

    private static int totalPage;

    public static boolean isNeedPage(){
        return pageSize>0&&pageNum>0;
    }

    public static void noPage(){
        pageNum = 0;
        pageSize = 0;
        totalRecord = 0;
        totalPage = 0;
    }

    public static void setPage(Integer pageNums, Integer pageSizes){
        if (pageNums!=null && pageSizes!=null) {
            pageNum = pageNums;
            pageSize = pageSizes;
        }
    }

    public static int getPageNum() {
        return pageNum;
    }

    public static void setPageNum(int pageNum) {
        pageNum = pageNum;
    }

    public static int getPageSize() {
        return pageSize;
    }

    public static void setPageSize(int pageSize) {
        pageSize = pageSize;
    }

    public static int getTotalRecord() {
        return totalRecord;
    }

    public static void setTotalRecord(int totalRecord) {
        totalRecord = totalRecord;
    }

    public static int getTotalPage() {
        return totalPage;
    }

    public static void setTotalPage(int totalPage) {
        totalPage = totalPage;
    }
}
