package com.smart5G.utils;

import lombok.Data;

import java.io.Serializable;

@Data
public class Result<T> implements Serializable {
    private T data;
    private String code;
    private String msg;

    static public Result<Object> success(Object t){
        Result<Object> result = new Result<>();
        result.code = "1000";
        result.msg="成功!";
        result.data = t;
        return result;
    }

    static public Result<Object> fail(String code,String msg){
        Result<Object> result = new Result<>();
        result.code = code;
        result.msg = msg;
        return result;
    }
}
