package com.smart5G.utils;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class PageResult<T> implements Serializable {
    private int pageNum = 1;

    private int pageSize = 5;

    private int totalRecord;

    private int totalPage;

    private List<T> data;

    private boolean success;

    private String errorMsg;

    public static PageResult success(Object data){
        PageResult model = new PageResult();
        model.success = true;
        model.data = (List<Object>)data;
        return model;
    }

    public static PageResult success(Object data, int pageNum, int pageSize, int totalPage, int totalRecord){
        PageResult model = new PageResult();
        model.success = true;
        model.data = (List<Object>)data;
        model.pageNum = pageNum;
        model.pageSize = pageSize;
        model.totalPage = totalPage;
        model.totalRecord = totalRecord;
        return model;
    }

    public static PageResult fail(String errorMsg){
        PageResult model = new PageResult();
        model.success = false;
        model.errorMsg = errorMsg;
        return model;
    }
}
