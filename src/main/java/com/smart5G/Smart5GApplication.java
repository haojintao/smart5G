package com.smart5G;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

import javax.annotation.Resource;

@SpringBootApplication
@ImportResource("classpath:pageUtil.xml")
public class Smart5GApplication {

	public static void main(String[] args) {
		SpringApplication.run(Smart5GApplication.class, args);
	}

}
