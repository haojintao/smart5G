package com.smart5G.dao;

import com.smart5G.model.Maintain;
import com.smart5G.model.MaintainExample;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

@Mapper
@Component
public interface MaintainMapper {
    int countByExample(MaintainExample example);

    int deleteByExample(MaintainExample example);

    int deleteByPrimaryKey(String maintainId);

    int insert(Maintain record);

    int insertSelective(Maintain record);

    List<Maintain> selectByExample(MaintainExample example);

    Maintain selectByPrimaryKey(String maintainId);

    int updateByExampleSelective(@Param("record") Maintain record, @Param("example") MaintainExample example);

    int updateByExample(@Param("record") Maintain record, @Param("example") MaintainExample example);

    int updateByPrimaryKeySelective(Maintain record);

    int updateByPrimaryKey(Maintain record);

    //自己添加
    @Select("SELECT COUNT(*) FROM maintain WHERE province=#{province}")
    BigDecimal getAllMaintenanceAmount(String province);

    @Select("SELECT device_brand as brand,count(*) as quantity FROM maintain WHERE province = #{province} GROUP BY device_brand")
    ArrayList<Map<String,Integer>> getBrandMaintenanceAmount(String province);


    @Select("SELECT DATE_FORMAT( maintain_date, '%Y%m%d' ) days, count( * ) count FROM maintain where service_id=#{serviceId} GROUP BY days ORDER BY days DESC LIMIT 0,7;")
    ArrayList<Map<String,Object>> getMaintenanceAmountByDay(String serviceId);

    @Select("SELECT DATE_FORMAT( maintain_date, '%Y%u' ) weeks, count( * ) count FROM maintain  where service_id=#{serviceId} GROUP BY weeks ORDER BY weeks DESC LIMIT 0,7;")
    ArrayList<Map<String,Object>> getMaintenanceAmountByWeek(String serviceId);

    @Select("SELECT DATE_FORMAT( maintain_date, '%Y%m' ) months, count( * ) count FROM maintain  where service_id=#{serviceId} GROUP BY months ORDER BY months DESC LIMIT 0,7;")
    ArrayList<Map<String,Object>> getMaintenanceAmountByMonth(String serviceId);

}