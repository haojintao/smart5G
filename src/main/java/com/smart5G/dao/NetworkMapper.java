package com.smart5G.dao;

import com.smart5G.model.Network;
import com.smart5G.model.NetworkExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

@Mapper
@Component
public interface NetworkMapper {
    int countByExample(NetworkExample example);

    int deleteByExample(NetworkExample example);

    int deleteByPrimaryKey(String networkId);

    int insert(Network record);

    int insertSelective(Network record);

    List<Network> selectByExample(NetworkExample example);

    Network selectByPrimaryKey(String networkId);

    int updateByExampleSelective(@Param("record") Network record, @Param("example") NetworkExample example);

    int updateByExample(@Param("record") Network record, @Param("example") NetworkExample example);

    int updateByPrimaryKeySelective(Network record);

    int updateByPrimaryKey(Network record);

    List<Network> queryNetwAndBrandByProvince(String province);
}