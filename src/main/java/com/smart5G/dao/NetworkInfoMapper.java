package com.smart5G.dao;

import com.smart5G.model.NetworkInfo;
import com.smart5G.model.NetworkInfoExample;
import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

@Mapper
@Component
public interface NetworkInfoMapper {
    int countByExample(NetworkInfoExample example);

    int deleteByExample(NetworkInfoExample example);

    int deleteByPrimaryKey(String infoId);

    int insert(NetworkInfo record);

    int insertSelective(NetworkInfo record);

    List<NetworkInfo> selectByExample(NetworkInfoExample example);

    NetworkInfo selectByPrimaryKey(String infoId);

    int updateByExampleSelective(@Param("record") NetworkInfo record, @Param("example") NetworkInfoExample example);

    int updateByExample(@Param("record") NetworkInfo record, @Param("example") NetworkInfoExample example);

    int updateByPrimaryKeySelective(NetworkInfo record);

    int updateByPrimaryKey(NetworkInfo record);

}